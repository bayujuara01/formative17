package com.ariefyantobayu.controller;

import com.ariefyantobayu.controller.ArisanController;
import com.ariefyantobayu.model.Member;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ArisanControllerTest {
	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	private final ArisanController ac = new ArisanController(150000);
	
	@BeforeEach
	void setUp() {
		System.setOut(new PrintStream(outputStream));
	}

	@Test
	void test() {
	
	}
	
	@Test
	@DisplayName("Start Arisan, Before Input a Member")
	void startBeforeInputMember() {
		ac.start();
		String expected = "INFOMinimal3anggotauntukmemulaiarisan".replaceAll("[^0-9a-zA-Z]+", "");
		String actual = outputStream.toString().replaceAll("[^0-9a-zA-Z]+", "");
		assertEquals(expected, actual);
	}
	
	@ParameterizedTest
	@DisplayName("Input Member, Before Start Arisan")
	@CsvSource(value = {"Bayu,Wirotaman", "Akbar,Surabaya", "Hilal,Bogor", "L,L"}, delimiter = ',')
	void inputMemberBeforeStart(String name, String address ) {
		ac.addMember(new Member(name, address));
		assertTrue(outputStream.toString().contains("Member Berhasil Ditambah"));
	}
	
	@Test
	@DisplayName("Arisan Initialization, Start")
	void startArisanInitialization() {
		ac.addMember(new Member("Bayu", "Wirotaman"));
		ac.addMember(new Member("Akbar","Surabaya"));
		ac.addMember(new Member("Anies", "Semarang"));
		ac.start();
		
		assertTrue(outputStream.toString().contains("INFO OPENING"));
	}
	
	@Test
	@DisplayName("Can't Add Member When Arisan Started Test")
	void failedAddMemberWhenArisanStarted() {
		ac.addMember(new Member("Bayu", "Wirotaman"));
		ac.addMember(new Member("Akbar","Surabaya"));
		ac.addMember(new Member("Anies", "Semarang"));
		ac.start();
		ac.addMember(new Member("Test", "Test"));
		
		assertTrue(outputStream.toString().contains("arisan sedang berlangsung"));
	}
	
	@Test
	@DisplayName("Failed Calculate Draw When Arisan Not Started Test")
	void failedCalculateDrawWhenNotStarted() {
		ac.computePrize();
		assertTrue(outputStream.toString().contains("Arisan Belum Dibuka"));
	}
	
	@Test
	@DisplayName("Is Not Arisan Finish Test")
	void isArisanNotFinishTest() {
		ac.addMember(new Member("Test", "Test"));
		assertFalse(ac.isFinish());
	}
	
	@AfterEach
	void setDown() {
		System.setOut(standardOut);
	}

}
