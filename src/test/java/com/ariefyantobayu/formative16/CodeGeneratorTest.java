package com.ariefyantobayu.formative16;

import static org.junit.jupiter.api.Assertions.*;

import com.ariefyantobayu.formative17.CodeGenerator;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CodeGeneratorTest {
	private final CodeGenerator codeGenerator = CodeGenerator.getInstance();

	@Test
	@DisplayName("Random Code Is Alphanumeric")
	void test() {
		assertTrue(codeGenerator.getRandomStrings().matches("[A-Za-z0-9]+"));
	}

}
