package com.ariefyantobayu.model;

public class Member {
	private String name;
	private String address;
	private String code;
	
	public Member (String name, String address) {
		this.name = name;
		this.address = address;
		this.code = "none";
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return String.format("Anggota {name: %s, code: %s, address: %s}\n", name, code, address);
	}
	
}