package com.ariefyantobayu.formative17;

import java.util.Random;

public class CodeGenerator {
	public final String ALPHANUMERIC = "abcdefghojklmnopqrstufwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	private static CodeGenerator codeGenerator = null;
	private Random random = new Random(System.currentTimeMillis());
	
	public static CodeGenerator getInstance () {
		if (codeGenerator == null) {
			codeGenerator = new CodeGenerator();
		}
		return codeGenerator;
	}
	
	public String getRandomStrings() {
		return getRandomStrings(12);
	}
	
	public String getRandomStrings(int length) {
		char[] code = new char[length];
		for (int i = 0; i < code.length; i++) {
			code[i] = ALPHANUMERIC.charAt(random.nextInt(ALPHANUMERIC.length()));
			
		}
		return String.valueOf(code);
	}
	
}
