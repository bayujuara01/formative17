package com.ariefyantobayu.formative17;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ariefyantobayu.controller.ArisanController;

@SpringBootApplication
public class Formative17Application {	
	public static void main(String[] args) {
		SpringApplication.run(ArisanController.class, args);	
	}

}
