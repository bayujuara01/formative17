package com.ariefyantobayu.view;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

import com.ariefyantobayu.model.Member;

public class ArisanView {
	public static String BANNER = "===============================\n";
	
	public void printInformation(LocalDate startDate, int installmentEach, ArrayList<Member> memberList) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("==============INFO=============\n");
		if (Objects.isNull(startDate)) {
			stringBuilder.append("      Arisan Belum Dimulai\n");
		} else {
			stringBuilder.append(String.format("Tanggal Mulai : %s\n", startDate));
			stringBuilder.append(String.format("Basar Iuran : Rp. %d\n", installmentEach));
			stringBuilder.append("Anggota : \n");

			for (Member member : memberList) {
				stringBuilder.append(member.toString());
			}
		}

		stringBuilder.append("================================");
		System.out.println(stringBuilder.toString());
	}
	
	public void printMenu() {
		System.out.println("1. Tambah Anggota\n2. Inisialisasi Arisan\n3. Kocok\n4. Info\n0. Keluar");
		System.out.print("Pilih : ");
	}
	
	public void printBanner(String title) {
		StringBuilder stringBuilder = new StringBuilder();
		
		for (int i = 0; i < Math.floor((double)(BANNER.length() - title.length()) / 2.0); i++) {
			stringBuilder.append("=");
		}
		stringBuilder.append(title);
		for (int i = 0; i < Math.ceil((double) (BANNER.length() - title.length()) / 2.0) - 1; i++) {
			stringBuilder.append("=");
		}
		
		stringBuilder.append('\n');
		System.out.print(stringBuilder.toString());
	}
	
	public void printBannerWithBody(String title, String body) {
		StringBuilder stringBuilder = new StringBuilder();
	
		stringBuilder.append(body + '\n');
		stringBuilder.append(BANNER);
		printBanner(title);
		System.out.print(stringBuilder.toString());
	}
	
	public void printWinner(Member member, int totalPrize) {
		String body = String.format("Selamat %s, dengan kode %s,\nmendapat : Rp. %d", 
				member.getName(), 
				member.getCode(),
				totalPrize
				);
		
		printBannerWithBody("PEMENANG", body);
	}
}
