package com.ariefyantobayu.controller;

import com.ariefyantobayu.formative17.CodeGenerator;
import com.ariefyantobayu.model.Member;
import com.ariefyantobayu.view.ArisanView;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

public class ArisanController {
	private final ArisanView arisanView = new ArisanView();
	private final Random random = new Random(System.currentTimeMillis());
	private final CodeGenerator codeGenerator = CodeGenerator.getInstance();
	private final Scanner scanner = new Scanner(System.in);
	public ArrayList<Member> memberList = new ArrayList<>(); 
	private ArrayList<Member> prizedMemberList = new ArrayList<>(); 
	private ArrayList<String> codes = new ArrayList<>();
	private LocalDate startDate = null;
	private int installmentEach;
	
	public ArisanController() {
		installmentEach = 150000;
		run(scanner);
	}
	
	public ArisanController(int installmentEach) {
		this.installmentEach = installmentEach;
	}
	
	public void run(Scanner scanner) {
		System.out.println("================================");
		System.out.println("=====APLIKASI ARISAN SIMPLE=====");
		System.out.println("================================");
		
		String menuSelectionString = "";
		boolean isMenuSelectionValid = false;
		
		do {
			arisanView.printMenu();
			menuSelectionString = scanner.nextLine();
			isMenuSelectionValid = menuSelectionString.matches("[0-4]");
			
			if (isMenuSelectionValid) {
				switch (menuSelectionString) {
				case "1":
					addNewMember();
					break;
				case "2":
					start();
					break;
				case "3":
					computePrize();
					break;
				case "4":
					getInfo();
					break;
				default:
					menuSelectionString = "0";
					break;
				}
			}
			
		} while (!isMenuSelectionValid || !menuSelectionString.equals("0"));
		
		scanner.close();
	}
	
	private void addNewMember() {
		String name = "";
		String address = "";
		
		System.out.print("Masukkan Nama : ");
		name = scanner.nextLine();
		System.out.print("Masukkan Alamat : ");
		address = scanner.nextLine();
		
		addMember(new Member(name, address));
	}
	
	public void start() {
		if (memberList.size() >= 3) {
			startDate = LocalDate.now();
			arisanView.printBannerWithBody(
					"INFO OPENING", 
					"Arisan Telah Dibuka,\nPada : " + startDate.toString());
		} else {
			arisanView.printBannerWithBody("INFO", "[Minimal 3 anggota, untuk memulai arisan]");
		}
	}
	
	public void addMember (Member member) {
		if (Objects.isNull(startDate)) {
			memberList.add(member);
			arisanView.printBannerWithBody("INFO", "[Member Berhasil Ditambah]");
		} else {
			arisanView.printBannerWithBody("INFO", "[Maaf Tidak dapat daftar member, arisan sedang berlangsung]");
		}
	}
	
	public void computePrize() {
		Member prizedMember = null;
		codes.clear();
		
		if (Objects.isNull(startDate)) {
			arisanView.printBannerWithBody(
					"INFO",
					"Arisan Belum Dibuka, Silahkan\ntambah anggota & pilih 1\nuntuk inisialisasi"
			);
			return;
		}
		
		for (int i = 0; i < memberList.size(); i++) {
			Member member = memberList.get(i);
			String code = codeGenerator.getRandomStrings();
			member.setCode(code);
			codes.add(code);
		}
		
		int numberOfWheel = random.nextInt(codes.size());
		
		for (int i = 0; i < memberList.size(); i++) {
			Member member = memberList.get(i);
			if (member.getCode().equals(codes.get(numberOfWheel))) {
				prizedMemberList.add(member);
				prizedMember = member;
				memberList.remove(i);
				break;
			}
			
		}

		codes.remove(numberOfWheel);
		
		int totalPrize = installmentEach * (memberList.size() + prizedMemberList.size());
		arisanView.printWinner(prizedMember, totalPrize);
		
		if (isFinish()) {
			arisanView.printBanner("ARISAN TELAH SELESAI");
		}
		
	}
	
	public void getInfo() {
		arisanView.printInformation(startDate, installmentEach, memberList);
	}
	
	public boolean isFinish() {
		boolean check = false;
		if (memberList.size() <= 0) {
			startDate = null;
			memberList = new ArrayList<Member>();
			check = true;
		}
		return check;
	}
	
}
